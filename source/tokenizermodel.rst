=================
Tokenizer Model
=================

1. Выкачать готовую модель можно по этой `ссылке <http://dn11.isa.ru:8080/doc-enc-data/tokenizer.bpe.96k_from_400M.spm.model>`_._

2. Запустить процесс обучения. Приводится снизу пример запуска скрипта, где в качестве текстов используются предложения из датасета, состоящего из статей Википедии.

::

	[ -z "$DICT_SIZE" ] && DICT_SIZE=80000
	[ -z "$SENTS_CNT" ] && SENTS_CNT=200100200
	[ -z "$MODEL" ] && MODEL=bpe

	base_name="${MODEL}_$((DICT_SIZE/1000))k_from_$((SENTS_CNT/1000/1000))M"
	spm_train --input=<(cut -f2 -d $'\t' sents.src),<(cut -f2 -d $'\t' sents.tgt),wiki_sents \
        	--pad_id=3 \
        	--control_symbols='<frgb>,<frge>,<docb>,<doce>' \
        	--model_prefix="${base_name}.spm" \
        	--vocab_size=$DICT_SIZE \
        	--character_coverage=0.9998 \
        	--model_type=$MODEL \
        	--input_sentence_size=$SENTS_CNT \
        	--max_sentence_length=2096 \
        	--shuffle_input_sentence=true \
        	--train_extremely_large_corpus=true \
        	--num_threads=18 &> "${base_name}.log"


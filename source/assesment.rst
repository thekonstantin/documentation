
Оценка качества обученной модели
================================

Для оценки качества работы обученной модели необходимо сделать следующее:

1) Скачать датасеты для оценки качества. Их надо положить в ту же папку, что и обучающие датасеты, т.е. ``train/data/docs`` для документов, ``train/data/sents`` для предложений.
Помимо тестовых данных, которые были отделены от обучающей выборки, есть еще набор датасетов используемых только для теста.:

 * `GWikiMatch <http://dn11.isa.ru:8080/doc-enc-data/datasets.gwikimatch.v1.tar.gz>`_, `GWikiMatch presegmented <http://dn11.isa.ru:8080/doc-enc-data/datasets.gwikimatch.v1.segmented.tar.gz>`_.
 * `Essays <http://dn11.isa.ru:8080/doc-enc-data/datasets.essay.ru-en.v2.tar.gz>`_, `Essays presegmented <http://dn11.isa.ru:8080/doc-enc-data/datasets.essay.ru-en.v2.segmented.tar.gz>`_.
 * `Essays for sent retrieval <http://dn11.isa.ru:8080/doc-enc-data/datasets.sents.essay.ru-en.v2.tar.gz>`_.

2) Создать персональную конфигурацию для запуска. Создать персональную конфигурацию персональная конфигурация должна лежать в папке ``eval/conf``, 
которую также как и ``train`` необходимо примонтировать при запуске докер-контейнера. Для теста можно использовать конфигурацию ``mini`` (см. пример запуска).

3) Пример запуска скрипта оценки. В ``outputs`` будет находится только что обученная модель.

::

	docker run  --gpus=1  --rm  -v $(pwd)/train/:/train/ -v $(pwd)/eval:/eval/ \
    		semvectors/doc_enc_train:0.0.10 \
    		run_eval +personal/common=mini \
    		doc_encoder.model_path=/train/outputs/2023-04-04/15-51-57/model.pt


.. Documentationdocenc documentation master file, created by
   sphinx-quickstart on Thu Apr  6 02:09:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================================================
Добро пожаловать на страницу документации для проекта semvectors!
=================================================================


QUICK START
==============================

В качестве данных нужно использовать тот же ``docs-mini``. Для скачивания можно использовать команду:

::

        curl -O dn11.isa.ru:8080/doc-enc-data/datasets.docs.mini.v1.tar.gz


Окружение docker
------------------------------------

1. ``docker pull semvectors/doc_enc:0.0.10``
2. В директории ``docs-mini`` ввести команду: ``find texts/ -name "*.txt" > files.txt``
3. Положить в эту же директорию: `model.pt <http://dn11.isa.ru:8080/doc-enc-data/models.temp.pt>`_.
4. Команда запуска:

::

        DMD=<path to docs-mini> docker run  --gpus=1  --rm  -v $DMD:/temp/ -w /temp  semvectors/doc_enc:0.0.10 \
        docenccli docs -i /temp/files.txt -o /temp/vecs -m /temp/model.pt

Нужно отметить, что вектора сохраняются с помощью функции ``numpy.savez`` вместе с именами файлов.

Ниже приведено описание некоторых флагов:

* "--output_dir" или "-o" - папка вывода. required=True
* "--model_path" или "-m" - путь к модельке. required=True
* "--gpu" или "-g" - номер GPU устройства. default=0.
* "--batch_size" или "-b" - размер "батча". default=1000



Tokenizer model
----------------------------------------------------
Перейдите на эту страницу :doc:`tokenizermodel` для получения большей информации.

Процесс обучения
----------------------------------------------------
Перейдите на эту страницу :doc:`learningprocess` для получения большей информации


Оценка качества работы модели
-------------------------------------------------------

Перейдите на эту страницу :doc:`assesment` для получения большей информации

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   

   outputt
   settings
   learningprocess
   assesment
   hel
   trudnp
   nix
   tokenizermodel

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Модуль вывода
======================================


В качестве данных нужно использовать тот же ``docs-mini``. Для скачивания можно использовать команду:

::

	curl -O dn11.isa.ru:8080/doc-enc-data/datasets.docs.mini.v1.tar.gz


Окружение docker
-------------------------

1. ``docker pull semvectors/doc_enc:0.0.10``
2. В директории ``docs-mini`` ввести команду: ``find texts/ -name "*.txt" > files.txt``
3. Положить в эту же директорию: `model.pt <http://dn11.isa.ru:8080/doc-enc-data/models.temp.pt>`_.
4. Команда запуска:

::

	DMD=<path to docs-mini> docker run  --gpus=1  --rm  -v $DMD:/temp/ -w /temp  semvectors/doc_enc:0.0.10 \ 
	docenccli docs -i /temp/files.txt -o /temp/vecs -m /temp/model.pt

Нужно отметить, что вектора сохраняются с помощью функции ``numpy.savez`` вместе с именами файлов.

Ниже приведено описание некоторых флагов:

* "--output_dir" или "-o" - папка вывода. required=True
* "--model_path" или "-m" - путь к модельке. required=True
* "--gpu" или "-g" - номер GPU устройства. default=0.
* "--batch_size" или "-b" - размер "батча". default=1000


Пример использование API библиотеки
-----------------------------------------

1) Создаем простейший проект с двумя файлами: ``Dockerfile``, ``src/enc.py``. 
Где содержимое ``Dockerfile`` выглядит так:

::

	FROM semvectors/doc_enc:0.0.10

	COPY src/ .

	ENTRYPOINT [ "python", "./enc.py" ]

А содержимое ``src/enc.py``:

::

	#!/usr/bin/env python3

	from doc_enc.doc_encoder import DocEncoder, DocEncoderConf

	conf = DocEncoderConf(
    		model_path='/temp/model.pt',
    		use_gpu=0
	)
	doc_encoder = DocEncoder(conf)

	docs = [['simple sentence','second sentence'], ['Sentences are already segmented']]

	result = doc_encoder.encode_docs(docs)
	print(result)

2) Собираем образ: ``docker build -t temp .``
3) Запускаем: ``docker run -v $DMD:/temp  --gpus=1  --rm temp:latest``


Описание API
------------------

* ``encode_sents`` - кодирует предложения в вектора.
* ``encode_sents_stream`` - кодирует поток предложений (представленных с помощью генератора) в векторы. ``sents_generator`` должен выдавать отправленный текст. Это также может привести к появлению дополнительных полей, которые будут возвращены вместе с векторами. Сначала он собирает предложения в пакет размером ``sents_batch_size``. Затем вызывает ``encode_sents`` для каждого пакета. Этот метод выдает тип текста предложения, отправленные векторы и дополнительные данные из генератора.
* ``encode_docs_from_path_list`` - Этот метод кодирует текст из path_list в векторы и возвращает их. Тексты должны быть предварительно сегментированы, т.е. каждое предложение должно быть размещено на отдельной строке.
* ``encode_docs_from_dir`` - Этот метод выполняет итерацию по файлам в каталоге и возвращает кортеж путей и векторное представление текстов. Тексты должны быть предварительно сегментированы, т.е. каждое предложение должно быть размещено на отдельной строке
* ``encode_docs`` - Закодирует список документов в векторное представление. Документ - это либо список предложений или текст, в котором каждое предложение помещается на новую строку.
* ``encode_docs_stream`` Наиболее общий метод, который принимает генератор идентификаторов документов и функцию `fetcher`. `fetcher` будет вызван для пакета идентификаторов, чтобы получить текст документа. Он должен возвращать список текстов вместе c текстовым индексом.


